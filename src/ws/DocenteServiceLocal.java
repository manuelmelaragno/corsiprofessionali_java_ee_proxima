package ws;

import javax.ejb.Local;

import response.ResponseDocenteDelete;
import response.ResponseDocenteInsert;
import response.ResponseDocenteSelectAll;

@Local
public interface DocenteServiceLocal {
	public ResponseDocenteInsert insertDocente(String nome, String cognome, String materia);

	public ResponseDocenteDelete deleteDocente(Integer idDocente);

	public ResponseDocenteSelectAll getAll();
}
