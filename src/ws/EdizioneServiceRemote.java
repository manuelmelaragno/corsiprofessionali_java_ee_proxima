package ws;

import javax.ejb.Remote;

import response.ResponseEdizioneDelete;
import response.ResponseEdizioneInsert;
import response.ResponseEdizioneSelectAll;

@Remote
public interface EdizioneServiceRemote {

	public ResponseEdizioneInsert insertEdizione(Integer nEdizione, Integer idCorso, Integer idDocente);

	public ResponseEdizioneDelete deleteEdizione(Integer idEdizione);

	public ResponseEdizioneSelectAll getAll();
}
