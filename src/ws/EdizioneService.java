package ws;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.jws.WebService;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.resteasy.util.HttpResponseCodes;

import dao.CorsoDao;
import dao.DocenteDao;
import dao.EdizioneDao;
import dto.DtoEdizione;
import model.Corso;
import model.Docente;
import model.Edizione;
import response.ResponseEdizioneDelete;
import response.ResponseEdizioneInsert;
import response.ResponseEdizioneSelectAll;

/**
 * Session Bean implementation class EdizioneService
 */
@WebService
@Stateless
@LocalBean
public class EdizioneService implements EdizioneServiceRemote, EdizioneServiceLocal {

	@PersistenceContext(unitName = "Hib4PU")
	private EntityManager em;

	/**
	 * Default constructor.
	 */
	public EdizioneService() {
	}

	@Override
	public ResponseEdizioneInsert insertEdizione(Integer nEdizione, Integer idCorso, Integer idDocente) {
		ResponseEdizioneInsert response = new ResponseEdizioneInsert();
		if (nEdizione != null && idCorso != null && idDocente != null) {
			Corso corso = CorsoDao.getById(em, idCorso);
			Docente docente = DocenteDao.getById(em, idDocente);
			Edizione edizione = new Edizione();
			edizione.setCorso(corso);
			edizione.setDocente(docente);
			edizione.setNumero(nEdizione);

			if (corso != null && docente != null && EdizioneDao.insertEdizione(em, edizione)) {
				response.setResponseCode(HttpResponseCodes.SC_OK);
				response.setResponseStatus("OK");
				response.setErrorDescription(null);
			} else if (corso != null && docente != null && !EdizioneDao.insertEdizione(em, edizione)) {
				response.setResponseCode(HttpResponseCodes.SC_INTERNAL_SERVER_ERROR);
				response.setResponseStatus("KO");
				response.setErrorDescription("Errore nell'inserimento della sede");
			} else {
				response.setResponseCode(HttpResponseCodes.SC_INTERNAL_SERVER_ERROR);
				response.setResponseStatus("KO");
				response.setErrorDescription("Sede o docente non trovato/a, controllare i dati e riprovare");
			}
		} else {
			response.setResponseCode(HttpResponseCodes.SC_BAD_REQUEST);
			response.setResponseStatus("KO");
			response.setErrorDescription("Dati non validi");
		}
		return response;
	}

	@Override
	public ResponseEdizioneDelete deleteEdizione(Integer idEdizione) {
		ResponseEdizioneDelete response = new ResponseEdizioneDelete();
		if (idEdizione != null) {
			if (EdizioneDao.deleteEdizione(em, idEdizione)) {
				response.setResponseCode(HttpResponseCodes.SC_OK);
				response.setResponseStatus("OK");
				response.setErrorDescription(null);
			} else {
				response.setResponseCode(HttpResponseCodes.SC_INTERNAL_SERVER_ERROR);
				response.setResponseStatus("KO");
				response.setErrorDescription("Errore nell'eliminazione della sede");
			}
		} else {
			response.setResponseCode(HttpResponseCodes.SC_BAD_REQUEST);
			response.setResponseStatus("KO");
			response.setErrorDescription("Dato non validi");
		}
		return response;
	}

	@Override
	public ResponseEdizioneSelectAll getAll() {
		ResponseEdizioneSelectAll response = new ResponseEdizioneSelectAll();
		List<DtoEdizione> dtoEdizioni = new ArrayList<DtoEdizione>();
		List<Edizione> edizioni = EdizioneDao.getAll(em);
		DtoEdizione dtoEdizione = null;

		for (Edizione edizione : edizioni) {
			dtoEdizione = new DtoEdizione();
			dtoEdizione.setNumero(edizione.getNumero());
			dtoEdizione.setIdEdizione(edizione.getIdEdizione());
			dtoEdizione.setCorso(edizione.getCorso());
			dtoEdizione.setDocente(edizione.getDocente());

			dtoEdizioni.add(dtoEdizione);
		}

		return response;
	}
}
