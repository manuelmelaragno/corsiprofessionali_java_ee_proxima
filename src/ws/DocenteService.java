package ws;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.jws.WebService;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.resteasy.util.HttpResponseCodes;

import dao.DocenteDao;
import dto.DtoDocente;
import model.Docente;
import response.ResponseDocenteDelete;
import response.ResponseDocenteInsert;
import response.ResponseDocenteSelectAll;

/**
 * Session Bean implementation class DocenteService
 */
@WebService
@Stateless
@LocalBean
public class DocenteService implements DocenteServiceRemote, DocenteServiceLocal {

	@PersistenceContext(unitName = "Hib4PU")
	private EntityManager em;

	/**
	 * Default constructor.
	 */
	public DocenteService() {

	}

	@Override
	public ResponseDocenteInsert insertDocente(String nome, String cognome, String materia) {
		ResponseDocenteInsert response = new ResponseDocenteInsert();
		Docente docente = new Docente();

		if (nome != null && !nome.trim().equals("") && cognome != null && !cognome.trim().equals("") && materia != null && !materia.trim().equals("")) {
			docente.setNome(nome);
			docente.setCognome(cognome);
			docente.setMateria(materia);
			if (DocenteDao.insertDocente(em, docente)) {
				response.setResponseCode(HttpResponseCodes.SC_OK);
				response.setResponseStatus("OK");
				response.setErrorDescription(null);
			} else {
				response.setResponseCode(HttpResponseCodes.SC_INTERNAL_SERVER_ERROR);
				response.setResponseStatus("KO");
				response.setErrorDescription("Errore nell'inserimento del docente, riprovare pi� tardi.");
			}
		} else {
			response.setResponseCode(HttpResponseCodes.SC_BAD_REQUEST);
			response.setResponseStatus("KO");
			response.setErrorDescription("Valori inseriti non validi.");
		}
		return response;
	}

	@Override
	public ResponseDocenteDelete deleteDocente(Integer idDocente) {
		ResponseDocenteDelete response = new ResponseDocenteDelete();
		if (idDocente != null) {
			if (DocenteDao.deleteDocente(em, idDocente)) {
				response.setResponseCode(HttpResponseCodes.SC_OK);
				response.setResponseStatus("OK");
				response.setErrorDescription(null);
			} else {
				response.setResponseCode(HttpResponseCodes.SC_INTERNAL_SERVER_ERROR);
				response.setResponseStatus("KO");
				response.setErrorDescription("Errore nell'eliminazione del docente, riprovare pi� tardi.");
			}
		} else {
			response.setResponseCode(HttpResponseCodes.SC_BAD_REQUEST);
			response.setResponseStatus("KO");
			response.setErrorDescription("Valori inseriti non validi.");
		}
		return response;
	}

	@Override
	public ResponseDocenteSelectAll getAll() {
		ResponseDocenteSelectAll response = new ResponseDocenteSelectAll();
		List<Docente> docenti = DocenteDao.getAll(em);
		List<DtoDocente> dtoDocenti = new ArrayList<DtoDocente>();
		DtoDocente dtoDocente = null;

		for (Docente docente : docenti) {
			dtoDocente = new DtoDocente();
			dtoDocente.setNome(docente.getNome());
			dtoDocente.setCognome(docente.getCognome());
			dtoDocente.setIdDocente(docente.getIdDocente());
			dtoDocente.setMateria(docente.getMateria());
			docenti.add(docente);
		}

		if (!dtoDocenti.isEmpty()) {
			response.setResponse(dtoDocenti);
			response.setResponseCode(HttpResponseCodes.SC_OK);
			response.setResponseStatus("OK");
			response.setErrorDescription(null);
		} else {
			response.setResponseCode(HttpResponseCodes.SC_OK);
			response.setResponseStatus("OK");
			response.setErrorDescription("Nessun docente trovato.");
		}

		return response;
	}

}
