package ws;

import javax.ejb.Remote;

import response.ResponseCorsoDelete;
import response.ResponseCorsoInsert;
import response.ResponseCorsoSelectAll;
import response.ResponseCorsoSelectById;
import response.ResponseCorsoUpdate;

@Remote
public interface CorsoServiceRemote {
	public ResponseCorsoInsert insertCorso(String nomeCorso);

	public ResponseCorsoDelete deleteCorso(Integer idCorso);

	public ResponseCorsoUpdate updateCorsoNome(Integer idCorso, String nuovoNome);

	public ResponseCorsoSelectById getCorsoById(Integer idCorso);

	public ResponseCorsoSelectAll getAllCorsi();
}
