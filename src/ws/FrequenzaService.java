package ws;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.jws.WebService;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.resteasy.util.HttpResponseCodes;

import dao.CorsoDao;
import dao.FrequenzaDao;
import dto.DtoFrequenza;
import model.Corso;
import model.Frequenza;
import response.ResponseFrequenzaDelete;
import response.ResponseFrequenzaInsert;
import response.ResponseFrequenzaSelectAll;

/**
 * Session Bean implementation class FrequenzaService
 */
@WebService
@Stateless
@LocalBean
public class FrequenzaService implements FrequenzaServiceRemote, FrequenzaServiceLocal {

	@PersistenceContext(unitName = "Hib4PU")
	private EntityManager em;

	/**
	 * Default constructor.
	 */
	public FrequenzaService() {

	}

	@Override
	public ResponseFrequenzaInsert insertFrequenza(Integer idCorso, Integer idStudente) {
		ResponseFrequenzaInsert response = new ResponseFrequenzaInsert();

		if (idCorso != null && idStudente != null) {
			Frequenza frequenza = new Frequenza();
			Corso corso = CorsoDao.getById(em, idCorso);
			frequenza.setCorso(corso);
			frequenza.setIdStudente(idStudente);

			if (FrequenzaDao.insertFrequenza(em, frequenza)) {
				response.setResponseCode(HttpResponseCodes.SC_OK);
				response.setResponseStatus("OK");
				response.setErrorDescription(null);
			} else {
				response.setResponseCode(HttpResponseCodes.SC_INTERNAL_SERVER_ERROR);
				response.setResponseStatus("KO");
				response.setErrorDescription("Errore nell'inserimento della frequenza");
			}
		} else {
			response.setResponseCode(HttpResponseCodes.SC_BAD_REQUEST);
			response.setResponseStatus("KO");
			response.setErrorDescription("Valori non validi");
		}

		return response;
	}

	@Override
	public ResponseFrequenzaDelete deleteFrequenza(Integer idFrequenza) {
		ResponseFrequenzaDelete response = new ResponseFrequenzaDelete();

		if (idFrequenza != null) {
			if (FrequenzaDao.deleteFrequenza(em, idFrequenza)) {
				response.setResponseCode(HttpResponseCodes.SC_OK);
				response.setResponseStatus("OK");
				response.setErrorDescription(null);
			} else {
				response.setResponseCode(HttpResponseCodes.SC_INTERNAL_SERVER_ERROR);
				response.setResponseStatus("KO");
				response.setErrorDescription("Errore nell'eliminazione della frequenza");
			}
		} else {
			response.setResponseCode(HttpResponseCodes.SC_BAD_REQUEST);
			response.setResponseStatus("KO");
			response.setErrorDescription("Valori non validi");
		}

		return response;
	}

	public ResponseFrequenzaSelectAll getAll() {
		ResponseFrequenzaSelectAll response = new ResponseFrequenzaSelectAll();
		List<DtoFrequenza> dtoFrequenze = new ArrayList<DtoFrequenza>();
		List<Frequenza> frequenze = FrequenzaDao.getAll(em);
		DtoFrequenza dtoFrequenza = null;

		for (Frequenza frequenza : frequenze) {
			dtoFrequenza = new DtoFrequenza();
			dtoFrequenza.setCorso(frequenza.getCorso());
			dtoFrequenza.setIdFrequenza(frequenza.getIdFrequenza());
			dtoFrequenza.setIdStudente(frequenza.getIdStudente());
			dtoFrequenze.add(dtoFrequenza);
		}
		if (!dtoFrequenze.isEmpty()) {
			response.setResponse(dtoFrequenze);
			response.setResponseCode(HttpResponseCodes.SC_OK);
			response.setResponseStatus("OK");
			response.setErrorDescription(null);
		} else {
			response.setResponseCode(HttpResponseCodes.SC_OK);
			response.setResponseStatus("OK");
			response.setErrorDescription("Non presenti liste in memoria");
		}
		return response;
	}

}
