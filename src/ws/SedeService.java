package ws;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.jws.WebService;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.resteasy.util.HttpResponseCodes;

import dao.SedeDao;
import dto.DtoSede;
import model.Sede;
import response.ResponseSedeDelete;
import response.ResponseSedeInsert;
import response.ResponseSedeSelectAll;

/**
 * Session Bean implementation class SedeService
 */
@WebService
@Stateless
@LocalBean
public class SedeService implements SedeServiceRemote, SedeServiceLocal {

	@PersistenceContext(unitName = "Hib4PU")
	private EntityManager em;

	/**
	 * Default constructor.
	 */
	public SedeService() {
	}

	@Override
	public ResponseSedeInsert insertSede(String nome, String luogo) {
		ResponseSedeInsert response = new ResponseSedeInsert();
		if (nome != null && !nome.trim().equals("") && luogo != null && !luogo.trim().equals("")) {
			Sede sede = new Sede();
			sede.setLuogo(luogo);
			sede.setNome(nome);

			if (SedeDao.insertSede(em, sede)) {
				response.setResponseCode(HttpResponseCodes.SC_OK);
				response.setResponseStatus("OK");
				response.setErrorDescription(null);
			} else {
				response.setResponseCode(HttpResponseCodes.SC_INTERNAL_SERVER_ERROR);
				response.setResponseStatus("KO");
				response.setErrorDescription("Errore nell'inserimento della sede");
			}
		} else {
			response.setResponseCode(HttpResponseCodes.SC_BAD_REQUEST);
			response.setResponseStatus("KO");
			response.setErrorDescription("Dati non validi");
		}
		return response;
	}

	@Override
	public ResponseSedeDelete deleteSede(Integer idSede) {
		ResponseSedeDelete response = new ResponseSedeDelete();
		if (idSede != null) {
			if (SedeDao.deleteSede(em, idSede)) {
				response.setResponseCode(HttpResponseCodes.SC_OK);
				response.setResponseStatus("OK");
				response.setErrorDescription(null);
			} else {
				response.setResponseCode(HttpResponseCodes.SC_INTERNAL_SERVER_ERROR);
				response.setResponseStatus("KO");
				response.setErrorDescription("Errore nell'eliminazione della sede");
			}
		} else {
			response.setResponseCode(HttpResponseCodes.SC_BAD_REQUEST);
			response.setResponseStatus("KO");
			response.setErrorDescription("Dati non validi");
		}
		return response;
	}

	@Override
	public ResponseSedeSelectAll getAll() {
		ResponseSedeSelectAll response = new ResponseSedeSelectAll();
		List<DtoSede> dtoSedi = new ArrayList<DtoSede>();
		List<Sede> sedi = SedeDao.getAll(em);
		DtoSede dtoSede = null;

		for (Sede sede : sedi) {
			dtoSede = new DtoSede();
			dtoSede.setIdSede(sede.getIdSede());
			dtoSede.setNome(sede.getNome());
			dtoSede.setLuogo(sede.getLuogo());
			// dtoSede.setSedeCorsos

			dtoSedi.add(dtoSede);
		}

		return response;
	}
}
