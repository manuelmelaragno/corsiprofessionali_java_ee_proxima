package ws;

import java.util.Date;

import javax.ejb.Remote;

import response.ResponseSedeCorsoDelete;
import response.ResponseSedeCorsoInsert;
import response.ResponseSedeCorsoSelectAll;

@Remote
public interface SedeCorsoServiceRemote {
	public ResponseSedeCorsoInsert insertSedeCorso(Integer idSede, Integer idCorso, Date dataInizio, Date dataFine);

	public ResponseSedeCorsoDelete deleteSedeCorso(Integer idSedeCorso);

	public ResponseSedeCorsoSelectAll getAll();
}
