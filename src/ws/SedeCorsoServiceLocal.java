package ws;

import java.util.Date;

import javax.ejb.Local;

import response.ResponseSedeCorsoDelete;
import response.ResponseSedeCorsoInsert;
import response.ResponseSedeCorsoSelectAll;

@Local
public interface SedeCorsoServiceLocal {
	public ResponseSedeCorsoInsert insertSedeCorso(Integer idSede, Integer idCorso, Date dataInizio, Date dataFine);

	public ResponseSedeCorsoDelete deleteSedeCorso(Integer idSedeCorso);

	public ResponseSedeCorsoSelectAll getAll();
}
