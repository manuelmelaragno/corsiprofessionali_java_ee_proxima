package ws;

import javax.ejb.Remote;

import response.ResponseSedeDelete;
import response.ResponseSedeInsert;
import response.ResponseSedeSelectAll;

@Remote
public interface SedeServiceRemote {

	public ResponseSedeInsert insertSede(String nome, String luogo);

	public ResponseSedeDelete deleteSede(Integer idSede);

	public ResponseSedeSelectAll getAll();
}
