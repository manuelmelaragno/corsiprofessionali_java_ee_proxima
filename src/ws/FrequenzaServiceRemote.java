package ws;

import javax.ejb.Remote;

import response.ResponseFrequenzaDelete;
import response.ResponseFrequenzaInsert;

@Remote
public interface FrequenzaServiceRemote {
	public ResponseFrequenzaInsert insertFrequenza(Integer idCorso, Integer idStudente);

	public ResponseFrequenzaDelete deleteFrequenza(Integer idFrequenza);
}
