package ws;

import javax.ejb.Remote;

import response.ResponseDocenteDelete;
import response.ResponseDocenteInsert;
import response.ResponseDocenteSelectAll;

@Remote
public interface DocenteServiceRemote {
	public ResponseDocenteInsert insertDocente(String nome, String cognome, String materia);

	public ResponseDocenteDelete deleteDocente(Integer idDocente);

	public ResponseDocenteSelectAll getAll();

}
