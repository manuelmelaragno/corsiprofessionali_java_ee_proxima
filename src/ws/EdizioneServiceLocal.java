package ws;

import javax.ejb.Local;

import response.ResponseEdizioneDelete;
import response.ResponseEdizioneInsert;
import response.ResponseEdizioneSelectAll;

@Local
public interface EdizioneServiceLocal {

	public ResponseEdizioneInsert insertEdizione(Integer nEdizione, Integer idCorso, Integer idDocente);

	public ResponseEdizioneDelete deleteEdizione(Integer idEdizione);

	public ResponseEdizioneSelectAll getAll();

}
