package ws;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.jws.WebService;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.resteasy.util.HttpResponseCodes;

import dao.CorsoDao;
import dto.DtoCorso;
import model.Corso;
import response.ResponseCorsoDelete;
import response.ResponseCorsoInsert;
import response.ResponseCorsoSelectAll;
import response.ResponseCorsoSelectById;
import response.ResponseCorsoUpdate;

/**
 * Session Bean implementation class CorsoService
 */
@WebService
@Stateless
@LocalBean
public class CorsoService implements CorsoServiceRemote, CorsoServiceLocal {

	@PersistenceContext(unitName = "Hib4PU")
	private EntityManager em;

	public CorsoService() {

	}

	@Override
	public ResponseCorsoInsert insertCorso(String nomeCorso) {
		ResponseCorsoInsert response = new ResponseCorsoInsert();

		Corso corso = new Corso();
		corso.setNome(nomeCorso);

		if (nomeCorso != null && !nomeCorso.trim().equals("")) {
			if (CorsoDao.insertCorso(em, corso)) {
				response.setResponseCode(HttpResponseCodes.SC_OK);
				response.setResponseStatus("OK");
				response.setErrorDescription(null);
			} else {
				response.setResponseCode(HttpResponseCodes.SC_INTERNAL_SERVER_ERROR);
				response.setResponseStatus("KO");
				response.setErrorDescription("Errore nell'inserimento del corso");
			}
		} else {
			response.setResponseCode(HttpResponseCodes.SC_BAD_REQUEST);
			response.setResponseStatus("KO");
			response.setErrorDescription("Nome non valido");
		}
		return response;
	}

	@Override
	public ResponseCorsoDelete deleteCorso(Integer idCorso) {
		ResponseCorsoDelete response = new ResponseCorsoDelete();

		if (idCorso != null) {
			if (CorsoDao.deleteCorso(em, idCorso)) {
				response.setResponseCode(HttpResponseCodes.SC_OK);
				response.setResponseStatus("OK");
				response.setErrorDescription(null);
			} else {
				response.setResponseCode(HttpResponseCodes.SC_INTERNAL_SERVER_ERROR);
				response.setResponseStatus("KO");
				response.setErrorDescription("Errore nell'eliminazione del corso");
			}
		} else {
			response.setResponseCode(HttpResponseCodes.SC_BAD_REQUEST);
			response.setResponseStatus("KO");
			response.setErrorDescription("Id non valido");
		}

		return response;
	}

	@Override
	public ResponseCorsoUpdate updateCorsoNome(Integer idCorso, String nuovoNome) {
		ResponseCorsoUpdate response = new ResponseCorsoUpdate();
		if ((nuovoNome != null && !nuovoNome.trim().equals("")) && (idCorso != null)) {
			Corso corso = new Corso();
			corso.setNome(nuovoNome);
			corso.setIdCorso(idCorso);
			if (CorsoDao.updateCorso(em, corso)) {
				response.setResponseCode(HttpResponseCodes.SC_OK);
				response.setResponseStatus("OK");
				response.setErrorDescription(null);
			} else {
				response.setResponseCode(HttpResponseCodes.SC_INTERNAL_SERVER_ERROR);
				response.setResponseStatus("KO");
				response.setErrorDescription("Errore nell'aggiornamento del corso");
			}
		} else {
			response.setResponseCode(HttpResponseCodes.SC_BAD_REQUEST);
			response.setResponseStatus("KO");
			response.setErrorDescription("Nome non valido");
		}
		return response;
	}

	public ResponseCorsoSelectById getCorsoById(Integer idCorso) {
		ResponseCorsoSelectById response = new ResponseCorsoSelectById();

		if (idCorso != null) {
			Corso corso = CorsoDao.getById(em, idCorso);
			if (corso != null) {
				DtoCorso dtoCorso = new DtoCorso();
				dtoCorso.setNome(corso.getNome());
				dtoCorso.setIdCorso(corso.getIdCorso());

				response.setResponseCode(HttpResponseCodes.SC_OK);
				response.setResponseStatus("OK");
				response.setErrorDescription(null);
				response.setResponse(dtoCorso);
			} else {
				response.setResponseCode(HttpResponseCodes.SC_INTERNAL_SERVER_ERROR);
				response.setResponseStatus("KO");
				response.setErrorDescription("Errore nella ricerca del corso");
			}
		} else {
			response.setResponseCode(HttpResponseCodes.SC_BAD_REQUEST);
			response.setResponseStatus("KO");
			response.setErrorDescription("Nome non valido");
		}

		return response;
	}

	@Override
	public ResponseCorsoSelectAll getAllCorsi() {
		ResponseCorsoSelectAll response = new ResponseCorsoSelectAll();
		List<Corso> corsi = new ArrayList<Corso>();
		List<DtoCorso> dtoCorsi = new ArrayList<DtoCorso>();

		corsi = CorsoDao.getAll(em);

		DtoCorso dtoCorso = null;
		for (Corso corso : corsi) {
			dtoCorso = new DtoCorso();
			dtoCorso.setNome(corso.getNome());
			dtoCorso.setIdCorso(corso.getIdCorso());

			dtoCorsi.add(dtoCorso);
		}

		response.setResponse(dtoCorsi);
		response.setResponseCode(HttpResponseCodes.SC_OK);
		response.setResponseStatus("OK");
		response.setErrorDescription(null);

		return response;
	}

}
