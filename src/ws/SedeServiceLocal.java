package ws;

import javax.ejb.Local;

import response.ResponseSedeDelete;
import response.ResponseSedeInsert;
import response.ResponseSedeSelectAll;

@Local
public interface SedeServiceLocal {
	public ResponseSedeInsert insertSede(String nome, String luogo);

	public ResponseSedeDelete deleteSede(Integer idSede);

	public ResponseSedeSelectAll getAll();
}
