package ws;

import javax.ejb.Local;

import response.ResponseFrequenzaDelete;
import response.ResponseFrequenzaInsert;

@Local
public interface FrequenzaServiceLocal {

	public ResponseFrequenzaInsert insertFrequenza(Integer idCorso, Integer idStudente);

	public ResponseFrequenzaDelete deleteFrequenza(Integer idFrequenza);

}
