package ws;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.jws.WebService;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.resteasy.util.HttpResponseCodes;

import dao.CorsoDao;
import dao.SedeCorsoDao;
import dao.SedeDao;
import dto.DtoSedeCorso;
import model.Corso;
import model.Sede;
import model.SedeCorso;
import response.ResponseSedeCorsoDelete;
import response.ResponseSedeCorsoInsert;
import response.ResponseSedeCorsoSelectAll;

/**
 * Session Bean implementation class SedeCorsoService
 */
@WebService
@Stateless
@LocalBean
public class SedeCorsoService implements SedeCorsoServiceRemote, SedeCorsoServiceLocal {

	@PersistenceContext(unitName = "Hib4PU")
	private EntityManager em;

	/**
	 * Default constructor.
	 */
	public SedeCorsoService() {
	}

	@Override
	public ResponseSedeCorsoInsert insertSedeCorso(Integer idSede, Integer idCorso, Date dataInizio, Date dataFine) {
		ResponseSedeCorsoInsert response = new ResponseSedeCorsoInsert();

		if (idSede != null && idCorso != null && dataInizio != null) {
			Corso corso = CorsoDao.getById(em, idCorso);
			Sede sede = SedeDao.getById(em, idSede);
			SedeCorso sedeCorso = new SedeCorso();
			sedeCorso.setCorso(corso);
			sedeCorso.setSede(sede);
			sedeCorso.setDataInizio(dataInizio);
			if (dataFine != null)
				sedeCorso.setDataFine(dataFine);
			if (corso != null && sede != null && SedeCorsoDao.insertSedeCorso(em, sedeCorso)) {
				response.setResponseCode(HttpResponseCodes.SC_OK);
				response.setResponseStatus("OK");
				response.setErrorDescription(null);
			} else if (corso != null && sede != null && !SedeCorsoDao.insertSedeCorso(em, sedeCorso)) {
				response.setResponseCode(HttpResponseCodes.SC_INTERNAL_SERVER_ERROR);
				response.setResponseStatus("KO");
				response.setErrorDescription("Errore nell'inserimento della riga");

			} else {
				response.setResponseCode(HttpResponseCodes.SC_INTERNAL_SERVER_ERROR);
				response.setResponseStatus("KO");
				response.setErrorDescription("Corso o sede non trovato/a, ricontrollare i dati e riprova");
			}
		} else {
			response.setResponseCode(HttpResponseCodes.SC_BAD_REQUEST);
			response.setResponseStatus("KO");
			response.setErrorDescription("Dati non validi");
		}

		return response;
	}

	@Override
	public ResponseSedeCorsoDelete deleteSedeCorso(Integer idSedeCorso) {
		ResponseSedeCorsoDelete response = new ResponseSedeCorsoDelete();

		if (idSedeCorso != null) {
			if (SedeCorsoDao.deleteSedeCorso(em, idSedeCorso)) {
				response.setResponseCode(HttpResponseCodes.SC_OK);
				response.setResponseStatus("OK");
				response.setErrorDescription(null);
			} else {
				response.setResponseCode(HttpResponseCodes.SC_INTERNAL_SERVER_ERROR);
				response.setResponseStatus("KO");
				response.setErrorDescription("Errore nell'eliminazione della riga");
			}
		} else {
			response.setResponseCode(HttpResponseCodes.SC_BAD_REQUEST);
			response.setResponseStatus("KO");
			response.setErrorDescription("Dati non validi");
		}

		return response;
	}

	@Override
	public ResponseSedeCorsoSelectAll getAll() {
		ResponseSedeCorsoSelectAll response = new ResponseSedeCorsoSelectAll();
		List<DtoSedeCorso> dtoSedeCorsi = new ArrayList<DtoSedeCorso>();
		List<SedeCorso> sedeCorsi = SedeCorsoDao.getAll(em);
		DtoSedeCorso dtoSedeCorso = null;

		for (SedeCorso sedeCorso : sedeCorsi) {
			dtoSedeCorso = new DtoSedeCorso();
			dtoSedeCorso.setCorso(sedeCorso.getCorso());
			dtoSedeCorso.setSede(sedeCorso.getSede());
			dtoSedeCorso.setIdSedeCorso(sedeCorso.getIdSedeCorso());
			dtoSedeCorso.setDataInizio(sedeCorso.getDataInizio());
			dtoSedeCorso.setDataFine(sedeCorso.getDataFine());

			dtoSedeCorsi.add(dtoSedeCorso);
		}

		return response;
	}
}
