package dto;

import java.util.Date;

import model.Corso;
import model.Sede;

public class DtoSedeCorso {

	private int idSedeCorso;

	private Date dataFine;

	private Date dataInizio;

	private Corso corso;

	private Sede sede;

	public int getIdSedeCorso() {
		return idSedeCorso;
	}

	public void setIdSedeCorso(int idSedeCorso) {
		this.idSedeCorso = idSedeCorso;
	}

	public Date getDataFine() {
		return dataFine;
	}

	public void setDataFine(Date dataFine) {
		this.dataFine = dataFine;
	}

	public Date getDataInizio() {
		return dataInizio;
	}

	public void setDataInizio(Date dataInizio) {
		this.dataInizio = dataInizio;
	}

	public Corso getCorso() {
		return corso;
	}

	public void setCorso(Corso corso) {
		this.corso = corso;
	}

	public Sede getSede() {
		return sede;
	}

	public void setSede(Sede sede) {
		this.sede = sede;
	}

}
