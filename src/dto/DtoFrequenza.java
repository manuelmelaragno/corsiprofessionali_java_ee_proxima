package dto;

import model.Corso;

public class DtoFrequenza {

	private int idFrequenza;

	private int idStudente;

	private Corso corso;

	public int getIdFrequenza() {
		return idFrequenza;
	}

	public void setIdFrequenza(int idFrequenza) {
		this.idFrequenza = idFrequenza;
	}

	public int getIdStudente() {
		return idStudente;
	}

	public void setIdStudente(int idStudente) {
		this.idStudente = idStudente;
	}

	public Corso getCorso() {
		return corso;
	}

	public void setCorso(Corso corso) {
		this.corso = corso;
	}

}
