package dto;

import model.Corso;
import model.Docente;

public class DtoEdizione {

	private int idEdizione;

	private int numero;

	private Corso corso;

	private Docente docente;

	public int getIdEdizione() {
		return idEdizione;
	}

	public void setIdEdizione(int idEdizione) {
		this.idEdizione = idEdizione;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public Corso getCorso() {
		return corso;
	}

	public void setCorso(Corso corso) {
		this.corso = corso;
	}

	public Docente getDocente() {
		return docente;
	}

	public void setDocente(Docente docente) {
		this.docente = docente;
	}

}
