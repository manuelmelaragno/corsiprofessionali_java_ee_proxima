package dto;

import java.util.List;

import model.SedeCorso;

public class DtoSede {

	private int idSede;

	private String luogo;

	private String nome;

	private List<SedeCorso> sedeCorsos;

	public int getIdSede() {
		return idSede;
	}

	public void setIdSede(int idSede) {
		this.idSede = idSede;
	}

	public String getLuogo() {
		return luogo;
	}

	public void setLuogo(String luogo) {
		this.luogo = luogo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<SedeCorso> getSedeCorsos() {
		return sedeCorsos;
	}

	public void setSedeCorsos(List<SedeCorso> sedeCorsos) {
		this.sedeCorsos = sedeCorsos;
	}

}
