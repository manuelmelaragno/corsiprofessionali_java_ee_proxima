package response;

import java.util.List;

import dto.DtoEdizione;

public class ResponseEdizioneSelectAll extends ResponseBase {

	private List<DtoEdizione> response;

	public List<DtoEdizione> getResponse() {
		return response;
	}

	public void setResponse(List<DtoEdizione> response) {
		this.response = response;
	}

}
