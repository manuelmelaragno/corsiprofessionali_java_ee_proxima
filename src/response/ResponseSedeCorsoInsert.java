package response;

import dto.DtoSedeCorso;

public class ResponseSedeCorsoInsert extends ResponseBase {

	private DtoSedeCorso response;

	public DtoSedeCorso getResponse() {
		return response;
	}

	public void setResponse(DtoSedeCorso response) {
		this.response = response;
	}

}
