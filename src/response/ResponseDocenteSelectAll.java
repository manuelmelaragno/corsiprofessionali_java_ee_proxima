package response;

import java.util.List;

import dto.DtoDocente;

public class ResponseDocenteSelectAll extends ResponseBase {

	private List<DtoDocente> response;

	public List<DtoDocente> getResponse() {
		return response;
	}

	public void setResponse(List<DtoDocente> response) {
		this.response = response;
	}

}
