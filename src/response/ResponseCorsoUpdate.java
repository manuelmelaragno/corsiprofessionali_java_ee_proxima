package response;

import dto.DtoCorso;

public class ResponseCorsoUpdate extends ResponseBase {

	private DtoCorso response;

	public DtoCorso getResponse() {
		return response;
	}

	public void setResponse(DtoCorso response) {
		this.response = response;
	}

}
