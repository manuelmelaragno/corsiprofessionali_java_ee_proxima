package response;

import java.util.List;

import dto.DtoFrequenza;

public class ResponseSedeSelectAll extends ResponseBase {

	private List<DtoFrequenza> response;

	public List<DtoFrequenza> getResponse() {
		return response;
	}

	public void setResponse(List<DtoFrequenza> response) {
		this.response = response;
	}

}
