package response;

import dto.DtoSedeCorso;

public class ResponseSedeCorsoDelete extends ResponseBase {

	private DtoSedeCorso response;

	public DtoSedeCorso getResponse() {
		return response;
	}

	public void setResponse(DtoSedeCorso response) {
		this.response = response;
	}

}
