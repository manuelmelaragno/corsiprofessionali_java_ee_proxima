package response;

import java.util.List;

import dto.DtoCorso;

public class ResponseCorsoSelectAll extends ResponseBase {

	private List<DtoCorso> response;

	public List<DtoCorso> getResponse() {
		return response;
	}

	public void setResponse(List<DtoCorso> response) {
		this.response = response;
	}

}
