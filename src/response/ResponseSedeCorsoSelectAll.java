package response;

import java.util.List;

import dto.DtoSedeCorso;

public class ResponseSedeCorsoSelectAll extends ResponseBase {

	private List<DtoSedeCorso> response;

	public List<DtoSedeCorso> getResponse() {
		return response;
	}

	public void setResponse(List<DtoSedeCorso> response) {
		this.response = response;
	}

}
