package response;

import dto.DtoDocente;

public class ResponseDocenteInsert extends ResponseBase {

	private DtoDocente response;

	public DtoDocente getResponse() {
		return response;
	}

	public void setResponse(DtoDocente response) {
		this.response = response;
	}

}
