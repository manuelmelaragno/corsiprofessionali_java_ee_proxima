package response;

import java.util.List;

import dto.DtoFrequenza;

public class ResponseFrequenzaSelectAll extends ResponseBase {

	private List<DtoFrequenza> response;

	public List<DtoFrequenza> getResponse() {
		return response;
	}

	public void setResponse(List<DtoFrequenza> response) {
		this.response = response;
	}

}
