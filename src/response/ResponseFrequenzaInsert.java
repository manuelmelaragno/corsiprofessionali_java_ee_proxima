package response;

import dto.DtoFrequenza;

public class ResponseFrequenzaInsert extends ResponseBase {

	private DtoFrequenza response;

	public DtoFrequenza getResponse() {
		return response;
	}

	public void setResponse(DtoFrequenza response) {
		this.response = response;
	}

}
