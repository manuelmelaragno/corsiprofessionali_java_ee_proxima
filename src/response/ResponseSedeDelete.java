package response;

import dto.DtoSede;

public class ResponseSedeDelete extends ResponseBase {

	private DtoSede response;

	public DtoSede getResponse() {
		return response;
	}

	public void setResponse(DtoSede response) {
		this.response = response;
	}

}
