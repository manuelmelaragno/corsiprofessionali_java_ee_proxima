package response;

import dto.DtoEdizione;

public class ResponseEdizioneDelete extends ResponseBase {

	private DtoEdizione response;

	public DtoEdizione getResponse() {
		return response;
	}

	public void setResponse(DtoEdizione response) {
		this.response = response;
	}

}
