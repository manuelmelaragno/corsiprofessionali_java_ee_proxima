package dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import model.Sede;

public class SedeDao {

	public static boolean insertSede(EntityManager em, Sede sede) {
		try {
			em.persist(sede);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public static boolean deleteSede(EntityManager em, Integer idSede) {
		try {
			Sede sede = em.find(Sede.class, idSede);
			if (sede != null) {
				em.remove(sede);
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	// TODO : update
	@SuppressWarnings("unchecked")
	public static List<Sede> getAll(EntityManager em) {
		List<Sede> sedi = new ArrayList<Sede>();

		sedi = em.createNamedQuery("Sede.findAll").getResultList();

		return sedi;
	}

	public static Sede getById(EntityManager em, Integer idSede) {
		Sede sede = null;

		sede = em.find(Sede.class, idSede);

		return sede;
	}
}
