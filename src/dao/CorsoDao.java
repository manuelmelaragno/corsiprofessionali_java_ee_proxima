package dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import model.Corso;

public class CorsoDao {

	public static boolean insertCorso(EntityManager em, Corso corso) {
		try {
			em.persist(corso);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public static boolean deleteCorso(EntityManager em, Integer idCorso) {
		try {
			Corso corso = em.find(Corso.class, idCorso);
			if (corso != null) {
				em.remove(corso);
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public static boolean updateCorso(EntityManager em, Corso corso) {
		Corso old = em.find(Corso.class, corso.getIdCorso());
		if (old != null) {
			if (corso.getEdiziones() == null || corso.getEdiziones().isEmpty()) {
				corso.setEdiziones(old.getEdiziones());
			}
			if (corso.getFrequenzas() == null || corso.getFrequenzas().isEmpty()) {
				corso.setFrequenzas(old.getFrequenzas());
			}
			if (corso.getNome() == null || corso.getNome().trim().equals("")) {
				corso.setNome(old.getNome());
			}
			if (corso.getSedeCorsos() == null || corso.getSedeCorsos().isEmpty()) {
				corso.setSedeCorsos(old.getSedeCorsos());
			}
			try {
				em.merge(corso);
				return true;
			} catch (Exception e) {
				e.printStackTrace();
				return false;
			}
		} else {
			return false;
		}

	}

	@SuppressWarnings("unchecked")
	public static List<Corso> getAll(EntityManager em) {
		List<Corso> corsi = new ArrayList<Corso>();

		corsi = em.createNamedQuery("Corso.findAll").getResultList();

		return corsi;
	}

	public static Corso getById(EntityManager em, Integer idCorso) {
		Corso corso = null;

		corso = em.find(Corso.class, idCorso);

		return corso;
	}
}
