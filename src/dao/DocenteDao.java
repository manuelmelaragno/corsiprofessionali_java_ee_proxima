package dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import model.Docente;

public class DocenteDao {

	public static boolean insertDocente(EntityManager em, Docente docente) {
		try {
			em.persist(docente);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public static boolean deleteDocente(EntityManager em, Integer idDocente) {
		Docente docente = em.find(Docente.class, idDocente);

		if (docente != null) {
			em.remove(docente);
			return true;
		} else {
			return false;
		}
	}
	// TODO : update

	@SuppressWarnings("unchecked")
	public static List<Docente> getAll(EntityManager em) {
		List<Docente> docenti = new ArrayList<Docente>();

		docenti = em.createNamedQuery("Docente.findAll").getResultList();

		return docenti;
	}

	public static Docente getById(EntityManager em, Integer idDocente) {
		Docente docente = null;

		docente = em.find(Docente.class, idDocente);

		return docente;
	}
}
