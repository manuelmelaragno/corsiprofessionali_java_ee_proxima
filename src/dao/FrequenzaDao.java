package dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import model.Frequenza;

public class FrequenzaDao {
	public static boolean insertFrequenza(EntityManager em, Frequenza frequenza) {
		try {
			em.persist(frequenza);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public static boolean deleteFrequenza(EntityManager em, Integer idFrequenza) {
		try {
			Frequenza frequenza = em.find(Frequenza.class, idFrequenza);
			if (frequenza != null) {
				em.remove(frequenza);
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	// TODO : update
	@SuppressWarnings("unchecked")
	public static List<Frequenza> getAll(EntityManager em) {
		List<Frequenza> frequenze = new ArrayList<Frequenza>();

		frequenze = em.createNamedQuery("Frequenza.findAll").getResultList();

		return frequenze;
	}

	public static Frequenza getById(EntityManager em, Integer idFrequenza) {
		Frequenza frequenza = null;

		frequenza = em.find(Frequenza.class, idFrequenza);

		return frequenza;
	}
}
