package dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import model.Edizione;

public class EdizioneDao {

	public static boolean insertEdizione(EntityManager em, Edizione edizione) {
		try {
			em.persist(edizione);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public static boolean deleteEdizione(EntityManager em, Integer idEdizione) {
		try {
			Edizione edizione = em.find(Edizione.class, idEdizione);
			if (edizione != null) {
				em.remove(edizione);
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	// TODO : update

	@SuppressWarnings("unchecked")
	public static List<Edizione> getAll(EntityManager em) {
		List<Edizione> edizioni = new ArrayList<Edizione>();

		edizioni = em.createNamedQuery("Edizione.findAll").getResultList();

		return edizioni;
	}

	public static Edizione getById(EntityManager em, Integer idEdizione) {
		Edizione edizione = null;

		edizione = em.find(Edizione.class, idEdizione);

		return edizione;
	}
}
