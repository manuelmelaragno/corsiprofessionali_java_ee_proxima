package dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import model.SedeCorso;

public class SedeCorsoDao {

	public static boolean insertSedeCorso(EntityManager em, SedeCorso sedeCorso) {
		try {
			em.persist(sedeCorso);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public static boolean deleteSedeCorso(EntityManager em, Integer idSedeCorso) {
		try {
			SedeCorso sedeCorso = em.find(SedeCorso.class, idSedeCorso);
			if (sedeCorso != null) {
				em.remove(sedeCorso);
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	// TODO : update
	@SuppressWarnings("unchecked")
	public static List<SedeCorso> getAll(EntityManager em) {
		List<SedeCorso> sedeCorsi = new ArrayList<SedeCorso>();

		sedeCorsi = em.createNamedQuery("SedeCorso.findAll").getResultList();

		return sedeCorsi;
	}

	public static SedeCorso getById(EntityManager em, Integer idSedeCorso) {
		SedeCorso sedeCorso = null;

		sedeCorso = em.find(SedeCorso.class, idSedeCorso);

		return sedeCorso;
	}
}
