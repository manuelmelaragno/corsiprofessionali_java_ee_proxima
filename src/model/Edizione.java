package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the edizione database table.
 * 
 */
@Entity
@NamedQuery(name="Edizione.findAll", query="SELECT e FROM Edizione e")
public class Edizione implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int idEdizione;

	private int numero;

	//bi-directional many-to-one association to Corso
	@ManyToOne
	@JoinColumn(name="idCorso")
	private Corso corso;

	//bi-directional many-to-one association to Docente
	@ManyToOne
	@JoinColumn(name="idDocente")
	private Docente docente;

	public Edizione() {
	}

	public int getIdEdizione() {
		return this.idEdizione;
	}

	public void setIdEdizione(int idEdizione) {
		this.idEdizione = idEdizione;
	}

	public int getNumero() {
		return this.numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public Corso getCorso() {
		return this.corso;
	}

	public void setCorso(Corso corso) {
		this.corso = corso;
	}

	public Docente getDocente() {
		return this.docente;
	}

	public void setDocente(Docente docente) {
		this.docente = docente;
	}

}