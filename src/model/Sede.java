package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the sede database table.
 * 
 */
@Entity
@NamedQuery(name="Sede.findAll", query="SELECT s FROM Sede s")
public class Sede implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int idSede;

	private String luogo;

	private String nome;

	//bi-directional many-to-one association to SedeCorso
	@OneToMany(mappedBy="sede")
	private List<SedeCorso> sedeCorsos;

	public Sede() {
	}

	public int getIdSede() {
		return this.idSede;
	}

	public void setIdSede(int idSede) {
		this.idSede = idSede;
	}

	public String getLuogo() {
		return this.luogo;
	}

	public void setLuogo(String luogo) {
		this.luogo = luogo;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<SedeCorso> getSedeCorsos() {
		return this.sedeCorsos;
	}

	public void setSedeCorsos(List<SedeCorso> sedeCorsos) {
		this.sedeCorsos = sedeCorsos;
	}

	public SedeCorso addSedeCorso(SedeCorso sedeCorso) {
		getSedeCorsos().add(sedeCorso);
		sedeCorso.setSede(this);

		return sedeCorso;
	}

	public SedeCorso removeSedeCorso(SedeCorso sedeCorso) {
		getSedeCorsos().remove(sedeCorso);
		sedeCorso.setSede(null);

		return sedeCorso;
	}

}