package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the frequenza database table.
 * 
 */
@Entity
@NamedQuery(name="Frequenza.findAll", query="SELECT f FROM Frequenza f")
public class Frequenza implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int idFrequenza;

	private int idStudente;

	//bi-directional many-to-one association to Corso
	@ManyToOne
	@JoinColumn(name="idCorso")
	private Corso corso;

	public Frequenza() {
	}

	public int getIdFrequenza() {
		return this.idFrequenza;
	}

	public void setIdFrequenza(int idFrequenza) {
		this.idFrequenza = idFrequenza;
	}

	public int getIdStudente() {
		return this.idStudente;
	}

	public void setIdStudente(int idStudente) {
		this.idStudente = idStudente;
	}

	public Corso getCorso() {
		return this.corso;
	}

	public void setCorso(Corso corso) {
		this.corso = corso;
	}

}