package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the sede_corso database table.
 * 
 */
@Entity
@Table(name="sede_corso")
@NamedQuery(name="SedeCorso.findAll", query="SELECT s FROM SedeCorso s")
public class SedeCorso implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int idSedeCorso;

	@Temporal(TemporalType.DATE)
	private Date dataFine;

	@Temporal(TemporalType.DATE)
	private Date dataInizio;

	//bi-directional many-to-one association to Corso
	@ManyToOne
	@JoinColumn(name="idCorso")
	private Corso corso;

	//bi-directional many-to-one association to Sede
	@ManyToOne
	@JoinColumn(name="idSede")
	private Sede sede;

	public SedeCorso() {
	}

	public int getIdSedeCorso() {
		return this.idSedeCorso;
	}

	public void setIdSedeCorso(int idSedeCorso) {
		this.idSedeCorso = idSedeCorso;
	}

	public Date getDataFine() {
		return this.dataFine;
	}

	public void setDataFine(Date dataFine) {
		this.dataFine = dataFine;
	}

	public Date getDataInizio() {
		return this.dataInizio;
	}

	public void setDataInizio(Date dataInizio) {
		this.dataInizio = dataInizio;
	}

	public Corso getCorso() {
		return this.corso;
	}

	public void setCorso(Corso corso) {
		this.corso = corso;
	}

	public Sede getSede() {
		return this.sede;
	}

	public void setSede(Sede sede) {
		this.sede = sede;
	}

}