package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the corso database table.
 * 
 */
@Entity
@NamedQuery(name="Corso.findAll", query="SELECT c FROM Corso c")
public class Corso implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int idCorso;

	private String nome;

	//bi-directional many-to-one association to Edizione
	@OneToMany(mappedBy="corso")
	private List<Edizione> ediziones;

	//bi-directional many-to-one association to Frequenza
	@OneToMany(mappedBy="corso")
	private List<Frequenza> frequenzas;

	//bi-directional many-to-one association to SedeCorso
	@OneToMany(mappedBy="corso")
	private List<SedeCorso> sedeCorsos;

	public Corso() {
	}

	public int getIdCorso() {
		return this.idCorso;
	}

	public void setIdCorso(int idCorso) {
		this.idCorso = idCorso;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Edizione> getEdiziones() {
		return this.ediziones;
	}

	public void setEdiziones(List<Edizione> ediziones) {
		this.ediziones = ediziones;
	}

	public Edizione addEdizione(Edizione edizione) {
		getEdiziones().add(edizione);
		edizione.setCorso(this);

		return edizione;
	}

	public Edizione removeEdizione(Edizione edizione) {
		getEdiziones().remove(edizione);
		edizione.setCorso(null);

		return edizione;
	}

	public List<Frequenza> getFrequenzas() {
		return this.frequenzas;
	}

	public void setFrequenzas(List<Frequenza> frequenzas) {
		this.frequenzas = frequenzas;
	}

	public Frequenza addFrequenza(Frequenza frequenza) {
		getFrequenzas().add(frequenza);
		frequenza.setCorso(this);

		return frequenza;
	}

	public Frequenza removeFrequenza(Frequenza frequenza) {
		getFrequenzas().remove(frequenza);
		frequenza.setCorso(null);

		return frequenza;
	}

	public List<SedeCorso> getSedeCorsos() {
		return this.sedeCorsos;
	}

	public void setSedeCorsos(List<SedeCorso> sedeCorsos) {
		this.sedeCorsos = sedeCorsos;
	}

	public SedeCorso addSedeCorso(SedeCorso sedeCorso) {
		getSedeCorsos().add(sedeCorso);
		sedeCorso.setCorso(this);

		return sedeCorso;
	}

	public SedeCorso removeSedeCorso(SedeCorso sedeCorso) {
		getSedeCorsos().remove(sedeCorso);
		sedeCorso.setCorso(null);

		return sedeCorso;
	}

}